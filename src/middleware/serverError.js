const log = require('node-utils').log('REQUEST');

// eslint-disable-next-line no-unused-vars
module.exports = () => (err, req, res, next) => {
  const message = 'Internal server error';
  log.error(message);
  return res.status(500).end(message);
};
