const log = require('node-utils').log('REQUEST');

module.exports = () => {
  const credentials = `${process.env.API_USERNAME}:${process.env.API_PASSWORD}`;

  return (req, res, next) => {
    if (req.method !== 'OPTIONS') {
      let key = req.get('authorization');
      if (key) {
        key = key.replace('Basic ', '');
        key = Buffer.from(key, 'base64').toString();
      }

      if (key !== credentials) {
        log.error(`Bad key '${key}' `);
        res.header('WWW-Authenticate', 'Basic realm="Node Server"');
        res.status(401);
        return res.end();
      }
    } else {
      return res.end();
    }

    return next();
  };
};
