module.exports = () => (req, res, next) => {
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Authorization, Content-Type');
  res.header('Access-Control-Allow-Credentials', 'true');

  if (process.env.ALLOWED_ORIGINS === '*') {
    res.header('Access-Control-Allow-Origin', '*');

  // Multiple origins are comma separated
  } else {
    const allowedOrigins = process.env.ALLOWED_ORIGINS.split(',');
    const index = allowedOrigins.indexOf(req.headers.origin);
    if (index !== -1) {
      res.header('Access-Control-Allow-Origin', allowedOrigins[index]);
    }
  }

  next();
};
