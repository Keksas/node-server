const log = require('node-utils').log('REQUEST');

module.exports = () => (req, res, next) => {
  log.info(`[${req.ip}] ${req.method} ${req.path}`);
  next();
};
