const log = require('node-utils').log('REQUEST');

module.exports = () => (err, req, res) => {
  const message = 'Validation error';
  log.error(message, err);
  return res.status(400).end(message);
};
