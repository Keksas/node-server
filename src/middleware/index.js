const Path = require('path');
const Utils = require('node-utils');

module.exports = (Server) => {
  const middleware = {};

  // Load default middleware
  Object.assign(middleware, Utils.fs.require(__dirname, [Server]));

  // Load consumer middleware
  if (Path.resolve('src/middleware') !== __dirname) {
    Object.assign(middleware, Utils.fs.require('middleware', [Server]));
  }

  return middleware;
};
