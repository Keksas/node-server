/**
 * Creates Sequelize instance to be used as a storage driver
 */

const path = require('path');
const Sequelize = require('sequelize');
const log = require('node-utils').log('DB');

const config = {
  // TODO: consider turn on logging if debug is on
  logging: false
};

let sequelize;
switch (process.env.DB_DIALECT) {
  case 'sqlite':
    if (!process.env.DB_STORAGE_PATH) {
      log.error('Missing environment variable DB_STORAGE_PATH', true);
    }
    config.dialect = process.env.DB_DIALECT;
    config.storage = path.resolve(process.env.DB_STORAGE_PATH);
    log.info(`Storage path: ${config.storage}`);
    sequelize = new Sequelize(config);
    break;

  case 'mysql':
  case 'mariadb':
  case 'postgres':
  case 'mssql':
    config.host = process.env.DB_HOST;
    config.port = process.env.DB_PORT;
    sequelize = new Sequelize(
      process.env.DB_DATABASE,
      process.env.DB_USERNAME,
      process.env.DB_PASSWORD,
      config
    );
    break;

  default:
    break;
}

module.exports = { Sequelize, sequelize };
