require('dotenv').config({ path: `${process.cwd()}/.env` });

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const Data = require('./data');
const Server = require('./server')();

const hooks = require('./hooks')(Server);
const middleware = require('./middleware')(Server);
const controllers = require('./controllers')(Server);
const routes = require('./routes');

// Log http requests
Server.express.use(middleware.log);

// Parse json requests
Server.express.use(bodyParser.json());

// Parse cookies
Server.express.use(cookieParser());

// Register modules with the server
Server.models = Data.models;
Server.middleware = middleware;
Server.controllers = controllers;

// Handle errors
Server.express.use(middleware.serverError);

// Register routes
Server.routes = routes(Server);

// Register hooks
Server.onStart = hooks.start;
Server.onShutdown = hooks.shutdown;

Data.migrate().then(() => {
  Server.start();
});
