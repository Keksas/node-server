/**
 * Loads models and configures migrations
 */

const Umzug = require('umzug');
const Driver = require('./driver');
const Utils = require('node-utils');

const log = Utils.log('DB');

// If no database driver was defined return stubs
// Allows to have applications that do not use sequelize
if (!Driver.sequelize) {
  module.exports = {
    migrate: () => Promise.resolve(),
    models: {}
  };
} else {
  let models = Utils.fs.require('models', [Driver]);
  const migrations = Utils.fs.resolvePath('migrations');

  // Pass models to relationship handler
  const relationshipModule = Utils.fs.resolvePath('relationships');
  try {
    models = require(relationshipModule)(models); // eslint-disable-line
  } catch (err) {
    log.warn(`If you need model relationships define them in ${relationshipModule}.js`);
  }

  // Migration config
  const config = {
    storage: 'sequelize',
    storageOptions: {
      sequelize: Driver.sequelize,
      modelName: 'migrations'
    },
    logging: log.info,
    migrations: {
      params: [Driver, models],
      path: migrations,
      pattern: /^\d+[\w-]+\.js$/
    }
  };

  const migrate = () => (new Promise((resolve) => {
    Utils.fs.readdir(migrations)
      .then(() => (new Umzug(config)).up().then(resolve))
      .catch(log.error)
    ;
  }));

  module.exports = { migrate, models };
}
