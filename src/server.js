const Utils = require('node-utils');
const https = require('https');
const http = require('http');
const express = require('express');
const fs = require('fs');
const path = require('path');

module.exports = () => {
  const self = this;

  self.Utils = Utils;
  self.Router = express.Router;
  self.express = express();

  self.host = process.env.HOST || '0.0.0.0';
  self.port = process.env.PORT || 3000;
  self.isDebug = process.env.DEBUG === 'true';
  self.isSecure = process.env.SLL_ENABLED === 'true';

  const log = Utils.log('SERVER');

  /**
   * Creates http or https server
   * @return {Object} http.Server or https.Server instance
   */
  const createServer = () => {
    if (self.isSecure) {
      return https.createServer({
        key: self.ssl_private_key,
        cert: self.ssl_cert,
        ca: self.ssl_ca,
        requestCert: process.env.SLL_CERT_REQUIRED === 'true',
        rejectUnauthorized: true
      }, self.express);
    }

    return http.createServer(self.express);
  };

  /*
   * Shutdown event handler
   * @param {string} sig
   */
  const shutdown = (sig) => {
    if (typeof sig === 'string') {
      if (typeof self.onShutdown === 'function') {
        self.onShutdown();
      }
      log.warn(`${sig} - server shutdown`);
      process.exit(0);
    }
  };

  /**
   * Loads ssl certificates and key
   * Kills the process if required environment variables are missing
   * Sets self.ssl_private_key, self.ssl_cert and self.ssl_ca
   * @return {[type]} [description]
   */
  const loadSSL = () => {
    ['SSL_PRIVATE_KEY', 'SSL_CERT', 'SSL_CA'].forEach((key) => {
      const config = process.env[key];
      if (!config) {
        log.error(`Environment variable ${key} is not defined!`);
        shutdown('SIGABRT');
      } else {
        self[key.toLowerCase()] = fs.readFileSync(config).toString();
      }
    });
  };

  /**
   * If exists sets public path
   * @return {undefined}
   */
  const registerPublicPath = () => {
    const publicPath = path.resolve(`${process.cwd()}/public`);

    try {
      fs.accessSync(publicPath);
    } catch (err) {
      log.warn(`Public path was not set because ${publicPath} does not exist`);
      return;
    }

    self.express.use(express.static(publicPath));
    log.info(`Public path is set to ${publicPath}`);
  };

  /**
   * Register view
   * @todo  Jade (pug) support should be added for this to make sence
   * @returns {undefined}
   */
  const registerViews = () => {
    const viewPath = path.resolve(`${__dirname}/views`);

    try {
      fs.accessSync(viewPath);
    } catch (err) {
      // log.warn(`No views were loaded because ${viewPath} does not exist`);
      return;
    }

    self.express.set('views', viewPath);
    log.info(`View path is set to ${viewPath}`);
  };

  /*
   * Register shutdown event handler
   */
  const setShutdownHandle = () => {
    process.on('exit', shutdown);
    [
      'SIGHUP', 'SIGINT', 'SIGQUIT',
      'SIGILL', 'SIGTRAP', 'SIGABRT',
      'SIGBUS', 'SIGFPE', 'SIGUSR1',
      'SIGSEGV', 'SIGUSR2', 'SIGTERM'
    ].forEach((element) => {
      process.on(element, () => shutdown(element));
    });
  };

  self.start = () => {
    setShutdownHandle();
    registerPublicPath();
    registerViews();

    if (self.isSecure) {
      loadSSL();
    }

    return createServer().listen(self.port, this.host, () => {
      log.info(`Listening on http${self.isSecure ? 's' : ''}://${self.host}:${self.port}`);
      if (typeof self.onStart === 'function') {
        self.onStart();
      }
    });
  };

  return self;
};
