const Joi = require('joi');
const Utils = require('node-utils');

const log = Utils.log('route');

module.exports = (Server) => {
  const Validate = {
    validate: (type, req, res, schema) => {
      if (!schema) {
        return Promise.resolve();
      }

      return new Promise((resolve, reject) => {
        Joi.validate(req[type], schema, err => (err ? reject(err.message) : resolve()));
      });
    },

    params: (req, res, schema) => Validate.validate('params', req, res, schema),

    body: (req, res, schema) => Validate.validate('body', req, res, schema)
  };

  const Router = (path, middleware, routes) => {
    const router = Server.Router();

    if (middleware.length) {
      middleware.forEach(item => router.use(item));
    }

    routes.forEach((route) => {
      router[route.method.toLowerCase()](route.path, (req, res) => {
        const validators = [];
        if (route.validation) {
          validators.push(Validate.params(req, res, route.validation.params));
          validators.push(Validate.body(req, res, route.validation.body));
        }

        Promise.all(validators)
          .then(() => route.handler(req, res))
          .catch(err => Server.middleware.validationError(err, req, res))
        ;
      });

      log.info(`${route.method} ${path}${route.path}`);
    });

    Server.express.use(path, router);
  };

  return Utils.fs.require('routes', [Server, Joi, Router]);
};
