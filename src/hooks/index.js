const Utils = require('node-utils');

module.exports = (Server) => {
  const hooks = Utils.fs.require('hooks', [Server]);
  return hooks || {};
};
