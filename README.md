* [Routes](#routes)
* [Middleware](#middleware)
* [Models](#models)
* [Migrations](#migrations)
* [Seeding](#seeding)
* [Hooks](#hooks)

# Usage

```shell
npm install --save node-server
cp ./node_modules/node-server/.env.example ./.env
# Edit ./.env settings
```

## Running the server

```shell
node_modules/.bin/node-server
```

## Keeping server running forever

[Process Manager (pm2)](https://github.com/Unitech/pm2) could be used for starting the server on boot and running it forever.

```shell
npm install pm2 -g

pm2 start service.js

# same but using npm start
pm2 start npm --name node-server -- start

pm2 log node-server
pm2 reload node-server # after code update if watch is not enabled
```

# Server

A server class is used to tie together different server components.

Server class properties are as follows:

| **Property**   | **Description**                                                 |
| -------------- | --------------------------------------------------------------- |
| App            | [Express instance](http://expressjs.com/en/4x/api.html#express) |
| Router         | [Express router](http://expressjs.com/en/4x/api.html#router)    |
| Utils          | Server utils for logging and module loading                     |
| models         | Gets populated from data/models                                 |
| middleware     | Gets populated from node-server/middleware and src/middleware   |

# Routes

Routes are defined in `src/routes/` and are used by [express router](https://expressjs.com/en/4x/api.html#router).

Every route module defines separate route group. This allows to have routes with different sets of middlware applied.

```javascript
// src/routes/root.js
module.exports = function(Server, Joi, Router) {
  return Router(
    // Path for the route group.
    // Every app should have root ('/') route defined
    '/',
    // Middleware
    [
      Server.middleware.cors
    ],
    // Route definitions
    [
      {
        method: 'GET',
        path: 'ping/',
        handler: Server.controllers.root.ping,
        // request params and body can be validated
        validation: {
          // validate json request body
          body: {},
          // validation schema for request parameters (eg /user/:id)
          params: {}
        }
      }
    ]
  );
};
```

## Route validation

Route validation is done using [joi](https://github.com/hapijs/joi).

# Controllers

Controllers are defined in `src/controllers`.

```javascript
// src/controlers/home.js
module.exports = function(Server) {
  return {
    ping: (req, res) => {
      res.end('pong');
    }
  };
};
```

# Middleware

#### Default middleware

Server comes with some predefined middleware. Some of it is applied globally (like log).

If global middleware is not desired it should be redefined in `src/middleware/` with the same name.

| **Name**        | **Description**                                                    |
| --------------- | ------------------------------------------------------------------ |
| cors            | Adds cross origin policy headers to response                       |
| log             | Logs request data (is applied globally)                            |
| secure          | Requires basic auth                                                |
| serverError     | Handles errors (is applied globally)                               |
| validationError | Handles validation errors (is applied when route validation fails) |

#### Custom middleware

Custom middleware can be registered in `src/middleware/`

```javascript
// end.js
module.exports = function(Server) {
  return (req, res, next) => {
    return Math.random() >= 0.5 ? res.end() : next();
  };
};
```

# Models

Models are based on [sequelize](http://docs.sequelizejs.com/en/v3/) and are stored in `data/models/`

```javascript
// user.js
module.exports = function(Driver) {
  return Driver.sequelize.define('user', {
    id: {
      type: Driver.Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: Driver.Sequelize.STRING
    }
  });
};
```

# Migrations

Migrations are managed by [sequelize/umzug](https://github.com/sequelize/umzug). They are stored in `data/migrations/` and are run on start up.

```javascript
// data/migrations/20161204154502-user.js
module.exports = {
  up: function(Driver, models) {
    return models.user.sync();
  },
  down: function(Driver, models) {
    return models.user.drop();
  }
};

```

If you need manual database schema manipulation use [queryInterface](http://docs.sequelizejs.com/en/latest/docs/migrations/#functions)

```javascript
// data/migrations/20170108203545-add-role-column.js
module.exports = {
  up: function (Driver, models) {
    Driver.sequelize.getQueryInterface().addColumn(models.user.getTableName(), 'roleId', {
      type: Driver.Sequelize.INTEGER,
      after: 'id'
    });
  },
  down: function (Driver, models) {
    Driver.sequelize.getQueryInterface().removeColumn(models.user.getTableName(), 'roleId');
  }
};
```

# Seeding

Seeds are run the same way as migrations

```javascript
// data/migrations/20161204154503-seed-user.js
module.exports = {
  up: function (Driver, models) {
    return models.user.bulkCreate([
        { name: 'John' },
        { name: 'Peter' }
    ]);
  },
  down: function (Driver, models) {
    return models.user.drop();
  }
};
```

# Hooks

Hooks are modules run when specific event occur on the server. Currently server support `start` and `shutdown` hooks. Hooks are registered in `src/hooks/`.

```javascript
// src/hooks/start.js
module.exports = function(Server) {
  return function() {
    Server.Utils.info('Server started!');
  };
};

```
