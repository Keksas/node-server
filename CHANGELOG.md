# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

### Added
- Unit tests
- LoggerInterface

### Changed
- Refactored utils

### Removed
- All code unrelated to this project. Left only logic for bootstrapping
